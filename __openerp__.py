# -*- coding: utf-8 -*-
{
    "name" : "Survey Matrix Text",
    'version' : '1.0',
    'description' : 'Dobtor matrix text for survery',
    'author' : "Steven, www.dobtor.com",
    'depends' : ['base','survey'],
    'data' : [
        'views/survery_templates.xml',
    ],
    'installable' : True,
}